﻿using SapNwRfc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetSAPInfo
{
    class DataSchema
    {
        public class CreateVendorParameters
        {
            [SapName("BUKRS")]
            public string BUKRS { get; set; }
            [SapName("NAME1")]
            public string NAME1 { get; set; }
            [SapName("REGIO")]
            public string REGIO { get; set; }
            [SapName("BANKL")]
            public string BANKL { get; set; }
            [SapName("BANKN")]
            public string BANKN { get; set; }
        }

        public class CreateVendorResult
        {
            [SapName("ZLIFNR")]
            public string ZLIFNR { get; set; }
            [SapName("ZFLAG")]
            public string ZFLAG { get; set; }
        }

        public class GetVendorInfoParameters
        {
            [SapName("ZBUKRS")]
            public string ZBUKRS { get; set; }
            [SapName("ZNAME")]
            public string ZNAME { get; set; }
        }

        public class GetVendorInfoResult
        {
            [SapName("ITAB")]
            public GetVendorInfoResultItem[] Items { get; set; }
        }

        public class GetVendorInfoResultItem
        {
            [SapName("ZLIFNR")]
            public string ZLIFNR { get; set; }
            [SapName("ZNAME")]
            public string ZNAME { get; set; }
        }


        public class CreatePurchaseOrderParameters
        {
            [SapName("PRHEADER")]
            public PrHeaderParametersItems PRHEADER { get; set; }
            [SapName("PUR_GROUP")]
            public string PUR_GROUP { get; set; }
            [SapName("PURCH_ORG")]
            public string PURCH_ORG { get; set; }
            [SapName("PREQ_NAME")]
            public string PREQ_NAME { get; set; }
            [SapName("PRITEM_EX")]
            public PrItemParametersItems[] PRITEM_EX { get; set; }
        }

        public class PrHeaderParametersItems
        {
            [SapName("PREQ_NO")]
            public string PREQ_NO { get; set; }
            [SapName("PR_TYPE")]
            public string PR_TYPE { get; set; }
            [SapName("CTRL_IND")]
            public string CTRL_IND { get; set; }
            [SapName("GENERAL_RELEASE")]
            public string GENERAL_RELEASE { get; set; }
            [SapName("CREATE_IND")]
            public string CREATE_IND { get; set; }
            [SapName("ITEM_INTVL")]
            public string ITEM_INTVL { get; set; }
            [SapName("LAST_ITEM")]
            public string LAST_ITEM { get; set; }
            [SapName("AUTO_SOURCE")]
            public string AUTO_SOURCE { get; set; }
            [SapName("MEMORY")]
            public string MEMORY { get; set; }
            [SapName("HOLD_COMPLETE")]
            public string HOLD_COMPLETE { get; set; }
            [SapName("HOLD_UNCOMPLETE")]
            public string HOLD_UNCOMPLETE { get; set; }
            [SapName("PARK_COMPLETE")]
            public string PARK_COMPLETE { get; set; }
            [SapName("PARK_UNCOMPLETE")]
            public string PARK_UNCOMPLETE { get; set; }
            [SapName("MEMORYTYPE")]
            public string MEMORYTYPE { get; set; }
        }

        public class PrItemParametersItems
        {
            [SapName("SHORT_TEXT")]
            public string SHORT_TEXT { get; set; }
            [SapName("PLANT")]
            public string PLANT { get; set; }
            [SapName("MATL_GROUP")]
            public string MATL_GROUP { get; set; }
            [SapName("QUANTITY")]
            public string QUANTITY { get; set; }
            [SapName("UNIT")]
            public string UNIT { get; set; }
            [SapName("DELIV_DATE")]
            public string DELIV_DATE { get; set; }
            [SapName("PREQ_PRICE")]
            public string PREQ_PRICE { get; set; }
            [SapName("PRICE_UNIT")]
            public decimal PRICE_UNIT { get; set; }
            [SapName("ACCTASSCAT")]
            public string ACCTASSCAT { get; set; }
            [SapName("GR_IND")]
            public string GR_IND { get; set; }
            [SapName("IR_IND")]
            public string IR_IND { get; set; }
            [SapName("DISTR_PERC")]
            public decimal DISTR_PERC { get; set; }
            [SapName("COSTCENTER")]
            public string COSTCENTER { get; set; }
            [SapName("ASSET_NO")]
            public string ASSET_NO { get; set; }
            [SapName("ORDERID")]
            public string ORDERID { get; set; }
            [SapName("GL_ACCOUNT")]
            public string GL_ACCOUNT { get; set; }
            [SapName("MATERIAL")]
            public string MATERIAL { get; set; }
            [SapName("PUR_GROUP")]
            public string PUR_GROUP { get; set; }
            [SapName("CURRENCY")]
            public string CURRENCY { get; set; }
            [SapName("PREQ_NAME")]
            public string PREQ_NAME { get; set; }
            [SapName("TRACKINGNO")]
            public string TRACKINGNO { get; set; }
            [SapName("STORE_LOC")]
            public string STORE_LOC { get; set; }
            [SapName("PURCH_ORG")]
            public string PURCH_ORG { get; set; }
            [SapName("WBS_ELEMENT")]
            public string WBS_ELEMENT { get; set; }
            [SapName("TEXT_LINE1")]
            public string TEXT_LINE1 { get; set; }
            [SapName("TEXT_LINE2")]
            public string TEXT_LINE2 { get; set; }
            [SapName("TEXT_LINE3")]
            public string TEXT_LINE3 { get; set; }
        }

        public class CreatePurchaseOrderResult
        {
            [SapName("E_TYPE")]
            public string E_TYPE { get; set; }
            [SapName("E_MESSAGE")]
            public string E_MESSAGE { get; set; }
            [SapName("NUMBER")]
            public string NUMBER { get; set; }
        }
    }
}
