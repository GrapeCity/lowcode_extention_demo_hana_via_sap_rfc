# 活字格低代码平台服务端编程Demo：通过SAP RFC操作HANA

#### 介绍
活字格低代码平台服务端编程Demo：通过SAP RFC操作NetWeaver API，实现对HANA中业务数据的读写。

#### 依赖项目
* [活字格服务端编程API](https://help.grapecity.com.cn/pages/viewpage.action?pageId=56534218)
* [SapNwRfc](https://github.com/huysentruitw/SapNwRfc)
